/* Openlights Strand Controller (12009) Firmware
 * http://openlights.org
 * 
 * Copyright (c) 2012 Jonathan Evans <jon@craftyjon.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <string.h>
#include <util/delay.h>

#include "globals.h"
#include "commands.h"
#include "rainbow.h"

/*
Blinks red and green board LED twice for 100ms to show the board is alive
*/
void blink_board_led() {
	PORTC.OUTCLR = PIN1_bm;	// DATA
	PORTC.OUTSET = PIN0_bm;	// ACT
	for (uint8_t j=0;j<2;j++) {		// number of times to blink LEDs

		PORTC.OUTTGL = PIN1_bm;	// DATA

		//PORTC_OUT = 0x01;		
		// turn on External LED on PC0, off LED on PC1
		delay_ms(100);			// delay
		
		PORTC.OUTTGL = PIN0_bm;	// ACT
		//PORTC_OUT = 0x02;		
		// turn off External LED on PC0, on LED on PC1
		delay_ms(100);			// delay
	}

// Leave both lights off for now (in case we missed them blink!)

	PORTC.OUTCLR = PIN1_bm;	// DATA
	PORTC.OUTCLR = PIN0_bm;	// ACT
	
}
void process_command()
{
	uint16_t i, j;

	switch (g_usbCommand) {
		case SET_PALETTE:
		   j = min(g_usbDataLength, (MAX_PALETTE));
		   for (i = 0; i < j; i++) {
			   palette[i] = Color(g_usbDataBuffer[i], g_usbDataBuffer[i+1],g_usbDataBuffer[i+2]);
		   }
		case SET_PAL_ARRAY:
			// printf("[CMD] Set Array\r\n");
			j = min(g_usbDataLength, (3 * num_leds));
			for (i = 0; i < j; i++) {
				setPixelColor(i, palette[i]);
			}
			break;
		case SET_ARRAY:
			// printf("[CMD] Set Array\r\n");
			j = min(g_usbDataLength, (3 * num_leds));
			for (i = 0; i < j; i++) {
				pixels[i] = g_usbDataBuffer[i];
			}
			dirty = 1;
			break;
		case SET_MAP:
			// printf("[CMD] Set Map\r\n");
			j = min(g_usbDataLength, (3 * num_leds));
			for (i = 0; i < j; i++) {
				setPixelColor(i, palette[i]);
			}
			break;
		case BLACKOUT:
			// printf("[CMD] Blackout\r\n");
			memset(&pixels, 0, sizeof(pixels));
			dirty = 1;
			break;

		case SET_ALL:
			// printf("[CMD] Set all\r\n");
			memset(&pixels, g_usbDataBuffer[0], sizeof(pixels));
			break;
			
		case SET_ALL_RGB:
			// printf("[CMD] Set All RGB\r\n");
			for (i = 0; i < (num_leds); i++) {
				setPixelColorI(i, g_usbDataBuffer[0], g_usbDataBuffer[1], g_usbDataBuffer[2]);
			}
			break;
		case SET_ALL_BEAT:
			for (i = 0; i < (num_leds); i++) {
				setPixelColorI(i, g_usbDataBuffer[0], g_usbDataBuffer[1], g_usbDataBuffer[2]);
			}
			break;
		case RAINBOW :
			rainbow(g_usbDataBuffer[0]);
			break;
		case WHEEL :
			rainbowCycle(g_usbDataBuffer[0]);
			break;
		case WATERWAVE :
			waterWave(g_usbDataBuffer[0]);
			break;
		case COLOURWIPE :
			colorWipe(Color(g_usbDataBuffer[0], g_usbDataBuffer[1], g_usbDataBuffer[2]), 20);
			break;
		case SET_NUM_LEDS:
			num_leds = g_usbDataBuffer[0] + (g_usbDataBuffer[1] << 8);
			identify();
			break;
		case BLINK_BOARD:
			blink_board_led();
			break;
		case GET_NUM_LEDS:
			break;	
		case IDENTIFY:
			// printf("[CMD] Identify\r\n");
			identify();
			break;
	}
	
}


void identify(void)
{
	memset(&pixels, 0, sizeof(pixels));
	pixels[1] = 0xFF;
	pixels[(3 * (num_leds - 1)) + 2] = 0xFF;
	
	// (2) for start/stop, (4) for address
	if (num_leds > 6) {
		for (uint16_t i = 0; i < 6; i++) {
			if (g_address & (1<<i)) {
				pixels[3 + (3 * i)] = 0xcc;
				pixels[3 + (3 * i) + 1] = 0xcc;
				pixels[3 + (3 * i) + 2] = 0xcc;
			} else {
				pixels[3 + (3 * i)] = 0x55;
				pixels[3 + (3 * i) + 1] = 0x00;
				pixels[3 + (3 * i) + 2] = 0x00;
			}
		}
	}

	dirty = 1;
}