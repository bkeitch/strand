/*
 * ws2811.c
 *
 * Created: 20/06/2013 19:52:14
 *  Author: bkeitch
 */ 
/*
Data sheet specifies: 

T0H 0 code, high voltage time = 0.5 �s � 150ns
T1H 1 code, high voltage time = 1.2 �s � 150ns
T0L 0 code, low voltage time  = 2.0 �s � 150ns
T1L 1 code, low voltage time  = 1.3 �s � 150ns
RES low voltage time Above 50�s

With the clock set to 16MHZ (T=62.5ns) we get the following:

High from 4 to 42 = 38 ticks = 38 * 31.25ns = 1.187us cf 1.2us
Low from 44 to 86 = 40 ticks = 42 *31.25ns = 1.3125us cf 1.3us
*/

#include "ws2811.h"
#include "globals.h"
/** Code below taken from ADA fruit site:
 https://github.com/adafruit/Adafruit_NeoPixel/blob/master/Adafruit_NeoPixel.cpp
 An alternative approach is available here:
 This is taken from some PIC code.
  http://www.insomnialighting.com/code/ws2811.c
 It assumes your SPI write is exactly 0.5us per cycle to meet WS2811 timing
  as suggested here : http://www.instructables.com/id/My-response-to-the-WS2811-with-an-AVR-thing/step4/Using-a-UART-with-out-external-inverter/

*/
void show(void) {

	
	// Not used : uint8_t pin = 1+14, // Output pin number C5
	uint8_t pinMask = _BV(5); // Output PORT bitmask C5
	volatile uint8_t *port = &PORTC; // Output PORT register
	
	if(!num_leds) return;

	volatile uint16_t
		i = num_bytes; // Loop counter
	volatile uint8_t
		*ptr = pixels, // Pointer to next byte
		b = *ptr++, // Current byte value
		hi, // PORT w/output bit set high
		lo; // PORT w/output bit set low

	// Data latch = 50+ microsecond pause in the output stream.
	// Rather than put a delay at the end of the function, the ending
	// time is noted and the function will simply hold off (if needed)
	// on issuing the subsequent round of data until the latch time has
	// elapsed. This allows the mainline code to start generating the
	// next frame of data rather than stalling for the latch.
	
	// we don't have a micros function (not Arduino) so for now this is commented out. Shouldn't matter, just to see data
	
	//while((micros() - endTime) < 50L);

	// endTime is a private member (rather than global var) so that
	// multiple instances on different pins can be quickly issued in
	// succession (each instance doesn't delay the next).

	// In order to make this code runtime-configurable to work with
	// any pin, SBI/CBI instructions are eschewed in favor of full
	// PORT writes via the OUT or ST instructions. It relies on two
	// facts: that peripheral functions (such as PWM) take precedence
	// on output pins, so our PORT-wide writes won't interfere, and
	// that interrupts are globally disabled while data is being issued
	// to the LEDs, so no other code will be accessing the PORT. The
	// code takes an initial 'snapshot' of the PORT state, computes
	// 'pin high' and 'pin low' values, and writes these back to the
	// PORT register as needed.

	cli(); // Disable interrupts; need 100% focus on instruction timing
	volatile uint8_t next, bit;
	// Choose whether we have inverted outputs or not
	// We're using an inverter to drive the LEDS...
	//lo = *port | pinMask;
	//hi = *port & ~pinMask;
#ifdef INVERTER_PRESENT
	lo = *port | pinMask;
	hi = *port & ~pinMask;
#else
	hi  = *port | pinMask;
	lo = *port & ~pinMask;
#endif
	next = lo;
	bit = 8;


	//asm volatile(
	//"head40:\n\t" // Clk Pseudocode (T = 0)
		//"st %a0, %1\n\t" // 2 PORT = hi (T = 2)
		//"sbrc %2, 7\n\t" // 1-2 if(byte & 128) ... then
			//"mov %4, %1\n\t" // 0-1 NEXT = hi (T = 4) .. else
			//"mul r0, r0\n\t" // 2 nop nop (T = 6)
			//"mul r0, r0\n\t" // 2 nop nop (T = 8)
			//"mul r0, r0\n\t" // 2 nop nop (T = 10'')
			//"st %a0, %4\n\t" // 2 PORT = next (T = 12)
			//"mul r0, r0\n\t" // 2 nop nop (T = 14)
			////	"mul r0, r0\n\t" // 2 nop nop (T = 16)
			////	"mul r0, r0\n\t" // 2 nop nop (T = 18)
			////	"mul r0, r0\n\t" // 2 nop nop (T = 20)
			//"nop\n\t" // 1 nop (T = 23)
		//"mov %4, %5\n\t" // 1 next = lo (T = 24)
		//"dec %3\n\t" // 1 bit-- (T = 25)
		//"breq nextbyte40\n\t" // 1-2 if(bit == 0)
		//"rol %2\n\t" // 1 b <<= 1 (T = 27)
		//"nop\n\t" // 1 nop (T = 28)
		//"mul r0, r0\n\t" // 2 nop nop (T = 30)
		//"st %a0, %5\n\t" // 2 PORT = lo (T = 34)
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 36)
		//"mul r0, r0\n\t" // 2 nop nop (T = 38)
	//"rjmp head40\n\t" // 2 -> head40 (next bit out)
		//"nextbyte40:\n\t" // (T = 27)
		//"ldi %3, 8\n\t" // 1 bit = 8 (T = 28)
		//"ld %2, %a6+\n\t" // 2 byte = *ptr++ (T = 30)
		//"st %a0, %5\n\t" // 2 PORT = lo (T = 34)
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 34'')
		//"mul r0, r0\n\t" // 2 nop nop (T = 36)
		//"sbiw %7, 1\n\t" // 2 i-- (T = 38)
	//"brne head40\n\t" // 1-2 if(i != 0) -> head40 (next byte)
	//::
	//"e" (port), // %a0
	//"r" (hi), // %1
	//"r" (b), // %2
	//"r" (bit), // %3
	//"r" (next), // %4
	//"r" (lo), // %5
	//"e" (ptr), // %a6
	//"w" (i) // %7
	//); // end asm
	 asm volatile(
	 "head40:"                  "\n\t" // Clk  Pseudocode    (T =  0)
	 "st   %a[port], %[hi]"    "\n\t" // 2    PORT = hi     (T =  2)
	 "sbrc %[byte] , 7"        "\n\t" // 1-2  if(b & 128)
	 "mov  %[next] , %[hi]"   "\n\t" // 0-1   next = hi    (T =  4)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T =  6)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T =  8)
	 "st   %a[port], %[next]"  "\n\t" // 2    PORT = next   (T = 10)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 12)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 14)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 16)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 18)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 20)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = *added*)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = *added*)
	 "st   %a[port], %[lo]"    "\n\t" // 2    PORT = lo     (T = 22)
	 "nop"                     "\n\t" // 1    nop           (T = 23)
	 "mov  %[next] , %[lo]"    "\n\t" // 1    next = lo     (T = 24)
	 "dec  %[bit]"             "\n\t" // 1    bit--         (T = 25)
	 "breq nextbyte40"         "\n\t" // 1-2  if(bit == 0)
	 "rol  %[byte]"            "\n\t" // 1    b <<= 1       (T = 27)
	 "nop"                     "\n\t" // 1    nop           (T = 28)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 30)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 32)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 34)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 36)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 38)
	 "rjmp head40"             "\n\t" // 2    -> head40 (next bit out)
	 "nextbyte40:"              "\n\t" //                    (T = 27)
	 "ldi  %[bit]  , 8"        "\n\t" // 1    bit = 8       (T = 28)
	 "ld   %[byte] , %a[ptr]+" "\n\t" // 2    b = *ptr++    (T = 30)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 32)
	 "st   %a[port], %[lo]"    "\n\t" // 2    PORT = lo     (T = 34)
	 "rjmp .+0"                "\n\t" // 2    nop nop       (T = 36)
	 "sbiw %[count], 1"        "\n\t" // 2    i--           (T = 38)
	 "brne head40"             "\n"   // 1-2  if(i != 0) -> (next byte)
	 : [port]  "+e" (port),
	 [byte]  "+r" (b),
	 [bit]   "+r" (bit),
	 [next]  "+r" (next),
	 [count] "+w" (i)
	 : [ptr]    "e" (ptr),
	 [hi]     "r" (hi),
	 [lo]     "r" (lo));
 
	sei(); // Re-enable interrupts
	
	// See above comment
	//endTime = micros(); // Note EOD time for latch on next call
}
void allOnes(void) {
	
	uint8_t pinMask = _BV(7); // Output PORT bitmask C7
	volatile uint8_t *port = &PORTC; // Output PORT register
	
	if(!num_leds) return;

	volatile uint16_t
	i = num_bytes*8; // Loop counter for all pixels
	volatile uint8_t
	*ptr = pixels, // Pointer to next byte
	b = 5,//*ptr++, // Current byte value
	hi, // PORT w/output bit set high
	lo; // PORT w/output bit set low

	cli(); // Disable interrupts; need 100% focus on instruction timing
	volatile uint8_t next, bit;

	lo = *port | pinMask; //Invert for the op-amp
	hi = lo & ~pinMask;
	next = lo;
	bit = 8;
	asm volatile(
	"ones100:\n\t" // Clk Pseudocode (T = 0)
	"st %a0, %1\n\t" // 2 set PORT = hi (T = 2)
	"mul r0, r0\n\t" // 2 nop nop (T = 4)
	"mul r0, r0\n\t" // 2 nop nop (T = 6)
	"mul r0, r0\n\t" // 2 nop nop (T = 8)
	"mul r0, r0\n\t" // 2 nop nop (T = 10)
	"mul r0, r0\n\t" // 2 nop nop (T = 12)
	"mul r0, r0\n\t" // 2 nop nop (T = 14)
	"mul r0, r0\n\t" // 2 nop nop (T = 16)
	"mul r0, r0\n\t" // 2 nop nop (T = 18)
	"mul r0, r0\n\t" // 2 nop nop (T = 20)
	"mul r0, r0\n\t" // 2 nop nop (T = 22)
	"mul r0, r0\n\t" // 2 nop nop (T = 24)
	"mul r0, r0\n\t" // 2 nop nop (T = 26)
	"mul r0, r0\n\t" // 2 nop nop (T = 28)
	"mul r0, r0\n\t" // 2 nop nop (T = 30)
	"mul r0, r0\n\t" // 2 nop nop (T = 32)
	"mul r0, r0\n\t" // 2 nop nop (T = 34)
	"mul r0, r0\n\t" // 2 nop nop (T = 36)
	"mul r0, r0\n\t" // 2 nop nop (T = 38)
	"mul r0, r0\n\t" // 2 nop nop (T = 40)
	"mul r0, r0\n\t" // 2 nop nop (T = 42)
	"mul r0, r0\n\t" // 2 nop nop (T = 44)
	"mul r0, r0\n\t" // 2 nop nop (T = 46)
	"mul r0, r0\n\t" // 2 nop nop (T = 48)	
	"mul r0, r0\n\t" // 2 nop nop (T = 46'')
	"mul r0, r0\n\t" // 2 nop nop (T = 48'')
	"nop \n\t"
	"st %a0, %4\n\t" // 2 PORT = next = lo (T = 50)

	"mul r0, r0\n\t" // 2 nop nop (T = 52)
	"mul r0, r0\n\t" // 2 nop nop (T = 54)
	"mul r0, r0\n\t" // 2 nop nop (T = 56)
	"mul r0, r0\n\t" // 2 nop nop (T = 58)
	"mul r0, r0\n\t" // 2 nop nop (T = 60)
	"mul r0, r0\n\t" // 2 nop nop (T = 62)
	"mul r0, r0\n\t" // 2 nop nop (T = 64)
	"mul r0, r0\n\t" // 2 nop nop (T = 66)
	"mul r0, r0\n\t" // 2 nop nop (T = 68)
	"mul r0, r0\n\t" // 2 nop nop (T = 70)
	"mul r0, r0\n\t" // 2 nop nop (T = 72)
	"mul r0, r0\n\t" // 2 nop nop (T = 74)
	"mul r0, r0\n\t" // 2 nop nop (T = 76)
	"mul r0, r0\n\t" // 2 nop nop (T = 78)
	"mul r0, r0\n\t" // 2 nop nop (T = 80)
	"mul r0, r0\n\t" // 2 nop nop (T = 82)
	"mul r0, r0\n\t" // 2 nop nop (T = 84)
	"mul r0, r0\n\t" // 2 nop nop (T = 86)
	"mul r0, r0\n\t" // 2 nop nop (T = 88)
	"mul r0, r0\n\t" // 2 nop nop (T = 90)
	"mul r0, r0\n\t" // 2 nop nop (T = 92)
	"mul r0, r0\n\t" // 2 nop nop (T = 94)
	"mul r0, r0\n\t" // 2 nop nop (T = 96)
	
	"sbiw %7, 1\n\t" // 2 i-- (T = 98)
	"brne ones100\n\t" // 1-2 if(i != 0) ->  (next byte)
	::
	"e" (port), // %a0
	"r" (hi), // %1
	"r" (b), // %2
	"r" (bit), // %3
	"r" (next), // %4
	"r" (lo), // %5
	"e" (ptr), // %a6
	"w" (i) // %7
	);
	sei(); // Re-enable interrupts
	
	// See above comment
	//endTime = micros(); // Note EOD time for latch on next call
}
/*
High from 4 to 42 = 38 ticks = 38 * 31.25ns = 1.187us cf 1.2us
Low from 44 to 86 = 40 ticks = 42 *31.25ns = 1.3125us cf 1.3us
*/
void allZeroes(void) {
/*
	"dec %3\n\t" // 2 bit-- (T = 72)
	"sbrc %3, 3\n\t" // 1-2 if(bit & 4) .. then
	"mov %3, %2\n\t" // 0-1 bit = b = 5 .. else 2 ticks less.
	*/	
	uint8_t pinMask = _BV(7); // Output PORT bitmask C7
	volatile uint8_t *port = &PORTC; // Output PORT register
	
	if(!num_leds) return;

	volatile uint16_t
	i = num_bytes * 8; // Loop counter number of pixels
	volatile uint8_t
	*ptr = pixels, // Pointer to next byte
	b = 5,//*ptr++, // Current byte value
	hi, // PORT w/output bit set high
	lo; // PORT w/output bit set low

	cli(); // Disable interrupts; need 100% focus on instruction timing
	volatile uint8_t next, bit;

	lo = *port | pinMask; //Invert for the op-amp
	hi = lo & ~pinMask;
	next = lo;
	bit = 8;
	asm volatile(
	"zero100:\n\t" // Clk Pseudocode (T = 0)
	"st %a0, %1\n\t" // 2 set PORT = hi (T = 2)
	"mul r0, r0\n\t" // 2 nop nop (T = 4)
	"mul r0, r0\n\t" // 2 nop nop (T = 6)
	"mul r0, r0\n\t" // 2 nop nop (T = 8)
	"mul r0, r0\n\t" // 2 nop nop (T = 10)
	"mul r0, r0\n\t" // 2 nop nop (T = 12)
	"mul r0, r0\n\t" // 2 nop nop (T = 14)
	"mul r0, r0\n\t" // 2 nop nop (T = 16)
	"mul r0, r0\n\t" // 2 nop nop (T = 18)
	"mul r0, r0\n\t" // 2 nop nop (T = 20)
	"mul r0, r0\n\t" // 2 nop nop (T = 18)
	"mul r0, r0\n\t" // 2 nop nop (T = 20)
	"nop \n\t"
	"st %a0, %4\n\t" // 2 PORT = next = lo (T = 22)	
	"mul r0, r0\n\t" // 2 nop nop (T = 24)
	"mul r0, r0\n\t" // 2 nop nop (T = 26)
	"mul r0, r0\n\t" // 2 nop nop (T = 28)
	"mul r0, r0\n\t" // 2 nop nop (T = 30)
	"mul r0, r0\n\t" // 2 nop nop (T = 32)
	"mul r0, r0\n\t" // 2 nop nop (T = 34)
	"mul r0, r0\n\t" // 2 nop nop (T = 36)
	"mul r0, r0\n\t" // 2 nop nop (T = 38)
	"mul r0, r0\n\t" // 2 nop nop (T = 40)
	"mul r0, r0\n\t" // 2 nop nop (T = 42)
	"mul r0, r0\n\t" // 2 nop nop (T = 44)
	"mul r0, r0\n\t" // 2 nop nop (T = 46)
	"mul r0, r0\n\t" // 2 nop nop (T = 48)
	"mul r0, r0\n\t" // 2 nop nop (T = 50)
	"mul r0, r0\n\t" // 2 nop nop (T = 52)
	"mul r0, r0\n\t" // 2 nop nop (T = 54)
	"mul r0, r0\n\t" // 2 nop nop (T = 56)
	"mul r0, r0\n\t" // 2 nop nop (T = 58)
	"mul r0, r0\n\t" // 2 nop nop (T = 60)
	"mul r0, r0\n\t" // 2 nop nop (T = 62)
	"mul r0, r0\n\t" // 2 nop nop (T = 64)
	"mul r0, r0\n\t" // 2 nop nop (T = 66)
	"mul r0, r0\n\t" // 2 nop nop (T = 68)
	"mul r0, r0\n\t" // 2 nop nop (T = 70)
	"mul r0, r0\n\t" // 2 nop nop (T = 72)
	"mul r0, r0\n\t" // 2 nop nop (T = 74)
	"mul r0, r0\n\t" // 2 nop nop (T = 76)
	"mul r0, r0\n\t" // 2 nop nop (T = 78)
	"mul r0, r0\n\t" // 2 nop nop (T = 80)
	"mul r0, r0\n\t" // 2 nop nop (T = 82)
	"mul r0, r0\n\t" // 2 nop nop (T = 84)
	"mul r0, r0\n\t" // 2 nop nop (T = 86)
	"mul r0, r0\n\t" // 2 nop nop (T = 88)
	"mul r0, r0\n\t" // 2 nop nop (T = 90)
	"mul r0, r0\n\t" // 2 nop nop (T = 92)
	"mul r0, r0\n\t" // 2 nop nop (T = 94)
	"mul r0, r0\n\t" // 2 nop nop (T = 96)
	"sbiw %7, 1\n\t" // 2 i-- (T = 98)
	"brne zero100\n\t" // 1-2 if(i != 0) ->  (next byte)
	::
	"e" (port), // %a0
	"r" (hi), // %1
	"r" (b), // %2
	"r" (bit), // %3
	"r" (next), // %4
	"r" (lo), // %5
	"e" (ptr), // %a6
	"w" (i) // %7
	);
	sei(); // Re-enable interrupts
}
/*
Outputs clock / 8 for clock testing
*/
void clock_test(void) {
	uint8_t pinMask = _BV(7); // Output PORT bitmask C7
	volatile uint8_t *port = &PORTC; // Output PORT register
	
	
	volatile uint16_t
	i =0; // Loop counter for all pixels
	volatile uint8_t
	*ptr = pixels, // Pointer to next byte
	b = 5,//*ptr++, // Current byte value
	hi, // PORT w/output bit set high
	lo; // PORT w/output bit set low

	cli(); // Disable interrupts; need 100% focus on instruction timing
	volatile uint8_t next, bit;

	lo = *port | pinMask; //Invert for the op-amp
	hi = lo & ~pinMask;
	next = lo;
	bit = 8;
	asm volatile(
	"clock8:\n\t" // Clk Pseudocode (T = 0)
	"st %a0, %1\n\t" // 2 set PORT = hi (T = 2)
	"mul r0, r0\n\t" // 2 nop nop (T = 4)	
	"st %a0, %4\n\t" // 2 PORT = next = lo (T = 6)
	"mul r0, r0\n\t" // 2 nop nop (T = 8)
	"rjmp clock8\n\t" // 1 loop (t = 8)
	::
	"e" (port), // %a0
	"r" (hi), // %1
	"r" (b), // %2
	"r" (bit), // %3
	"r" (next), // %4
	"r" (lo), // %5
	"e" (ptr), // %a6
	"w" (i) // %7
	);
	//never reached; infinte loop
	sei(); // Re-enable interrupts

}