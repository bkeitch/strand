/*
 * rainbow.h
 *
 * Created: 20/06/2013 20:30:10
 *  Author: bkeitch
 */ 


#ifndef RAINBOW_H_
#define RAINBOW_H_
#include <inttypes.h>
#include "globals.h"

void setPixelColorI(uint16_t n, uint8_t r, uint8_t g, uint8_t b);
void setPixelColor(uint16_t n, color_t c);
void colorTest(void);
void colorWipe(color_t c, uint8_t wait);
void rainbow(uint8_t wait);
void waterWave(uint8_t wait);
void rainbowCycle(uint8_t wait);
color_t Wheel(uint8_t WheelPos);
color_t Color(uint8_t r, uint8_t g, uint8_t b);
#endif /* RAINBOW_H_ */