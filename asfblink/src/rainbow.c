/*
 * rainbow.c
 *
 * Created: 20/06/2013 20:26:35
 *  Author: bkeitch
 */ 
#include "rainbow.h"
#include "globals.h"
#include "ws2811.h"

/*
Uses BRG color space
*/

// Set pixel color from 'packed' 32-bit RGB color
void setPixelColor(uint16_t n, color_t c) {
	if(n < num_leds) {
		uint8_t *p = &pixels[n * 3];
		*p++ = c >> 16;
		*p++ = c >> 8;
		*p = c;
	}
}

// Convert separate R,G,B into packed 32-bit RGB color.
// Packed format is always RGB, regardless of LED strand color order.
color_t Color(uint8_t r, uint8_t g, uint8_t b) {
	return ((uint32_t)b << 16) | ((uint32_t)r << 8) | g;
}
// Set pixel color from separate RGB 8-bit values
void setPixelColorI(uint16_t n, uint8_t r, uint8_t g, uint8_t b) {
	setPixelColor(n, Color(r,g,b));
}


// Some example procedures showing how to display to the pixels:
void colorTest() {
	colorWipe(Color(255, 0, 0), 50); // Red
	colorWipe(Color(0, 255, 0), 50); // Green
	colorWipe(Color(0, 0, 255), 50); // Blue
	rainbow(20);
	rainbowCycle(20);
}

// Fill the pixels one after the other with a color
void colorWipe(color_t c, uint8_t wait) {
	for(uint16_t i=0; i<num_leds; i++) {
		setPixelColor(i, c);
		show();
		delay_ms(wait);
	}
}
void waterWave(uint8_t wait) {
	color_t blue = Color(0,64,128);
	color_t turquoise =  Color(0,128,64);
	color_t green = Color(0,164,32);
	color_t off = (color_t) 0;
	
	for(uint16_t i=0; i<num_leds-5; i++) {
		setPixelColor(i, off);
		setPixelColor(i+1, green);
		setPixelColor(i+2, turquoise);
		setPixelColor(i+3, blue);
		setPixelColor(i+4, turquoise);
		setPixelColor(i+5, green);
		show();
		delay_ms(wait);
	}
	for(uint16_t i=num_leds;  i > 5; i--) {
		setPixelColor(i, off);
		setPixelColor(i-1, green);
		setPixelColor(i-2, turquoise);
		setPixelColor(i-3, blue);
		setPixelColor(i-4, turquoise);
		setPixelColor(i-5, green);
		show();
		delay_ms(wait);
	}
}
void rainbow(uint8_t wait) {
	uint16_t i, j;

	for(j=0; j<256; j++) {
		for(i=0; i<num_leds; i++) {
			setPixelColor(i, Wheel((i+j) & 255));
		}
		show();
		delay_ms(wait);
	}
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
	uint16_t i, j;

	for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
		for(i=0; i< num_leds; i++) {
			setPixelColor(i, Wheel(((i * 256 / num_leds) + j) & 255));
		}
		show();
		delay_ms(wait);
	}
}

// Input a value 0 to 255 to get a color value.
// The colors are a transition r - g - b - back to r.
color_t Wheel(uint8_t WheelPos) {
	if(WheelPos < 85) {
		return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
	} else if(WheelPos < 170) {
		WheelPos -= 85;
		return Color(255 - WheelPos * 3, 0, WheelPos * 3);
	} else {
		WheelPos -= 170;
		return Color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
}