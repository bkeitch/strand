/*
 * ws2811.h
 *
 * Created: 20/06/2013 19:52:31
 *  Author: bkeitch
 */ 


#ifndef WS2811_H_
#define WS2811_H_
#include <inttypes.h>

void show(void) ;
void allZeroes(void);
void allOnes(void);
void clock_test(void);

#endif /* WS2811_H_ */