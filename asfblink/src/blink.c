/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
//#include <asf.h>
//#define F_CPU 16000000
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
void flash() {
	int8_t  j;

	for (j=0;j<4;j++) {		// number of times to blink LEDs

		PORTC_OUT = 0x01;		// turn on External LED on PC0, off LED on PC1
		_delay_ms(50);			// delay
		
		PORTC_OUT = 0x02;		// turn off External LED on PC0, on LED on PC1
		_delay_ms(50);			// delay
	}
	PORTC_OUT = 0x00;		// turn off External LEDs on PC0, PC1
}
int main(void) {
//	board_init();

	//PORTR.DIRSET =	0x02;			//Debug Led
	PORTC.DIRSET =	0x03;			//External LEDs on PC0 & PC1
	PORTB.DIRCLR =	0xFF;			//Set port B as inputs
	PORTB.DIR    =	0x00;			//Set port B as inputs
	PORTB.PIN0CTRL = 0x18;			//set pin 0 as a pull-up
	PORTB.PIN1CTRL = 0x18;			//set pin 1 as a pull-up
	PORTB.PIN2CTRL = 0x18;			//set pin 2 as a pull-up

	PORTA.DIRCLR =	0xFF ;			//Set port A 
	PORTA.DIR=	0x00 ;			//Set port A 7,6,5,4 and 3 as inputs
	PORTA.PIN3CTRL = 0x18;			//set pin 3 as a pull-up
	PORTA.PIN4CTRL = 0x18;			//set pin 4 as a pull-up
	PORTA.PIN5CTRL = 0x18;			//set pin 5 as a pull-up
	PORTA.PIN6CTRL = 0x18;			//set pin 6 as a pull-up
	PORTA.PIN7CTRL = 0x18;			//set pin 7 as a pull-up


	flash();

	
	while(1) {				// main Loop
		if(((~PORTB_IN & (1<<0)) != 0 )
		|| ((~PORTA_IN & (1<<4)) != 0 )) { //~XXXXXXX1 & 00000100 = xxxx01
			PORTC_OUT = 0x01;		// turn on External LED on PC0, off LED on PC1
		} 
		if(((~PORTB_IN & (1<<1)) != 0 ) 
		 || ((~PORTA_IN & (1<<5)) != 0 )) {
			PORTC_OUT = 0x02;		// turn on External LED on PC1, off LED on PC0
		} 
		if(((~PORTB_IN & (1<<2)) != 0 )
		 || ((~PORTA_IN & (1<<6)) != 0 )) {
			PORTC_OUT = 0x03;		// turn on External LED on PC1 and n PC0
		}
		if(((~PORTA_IN & (1<<3)) != 0 )
		 || ((~PORTA_IN & (1<<7)) != 0 )) {
			flash();		// turn on External LED on PC1 and n PC0
		} 
	}
}
/*
int main(void) {
//	board_init();
//  XMEGA clock initialization to 32MHz
	CCP			= 0xD8;					// Configuration Change Protection: IOREG (0xD8)
	CLK_PSCTRL	= 0x00;					// System Clock Prescaler Reg All prescalers No Division
	OSC_CTRL	= 0x02;					// Bit 1- RC32MEN: Setup 32 MHz Internal CPU RC Oscillator Enable
	while ((OSC_STATUS & 0x02) == 0);	// wait for Bit 1 - RC32MRDY: 32 MHz Internal RC Oscillator Ready
	CCP			= 0xD8;					// Configuration Change Protection: IOREG (0xD8)
	CLK_CTRL	= 0x01;					// Bit 2:0 - SCLKSEL[2:0]: System Clock Selection

	PORTR.DIRSET =	0x02;			//Debug Led
	PORTC.DIRSET =	0x03;			//External LEDs on PC0 & PC1

	int16_t  j=0;

	for (j=0;j<10;j++) {	// number of times to blink LEDs

		PORTR.OUTCLR = 0x02;	// Turns the debug led on.  The led is active low
		PORTC_OUT = 0x01;		// turn on External LED on PC0, off LED on PC1
		_delay_ms(1000);		// delay	
			
		PORTR.OUTSET = 0x02;	// Turns the debug led off.  The led is active low
		PORTC_OUT = 0x02;		// turn off External LED on PC0, on LED on PC1
		_delay_ms(1000);		// delay	
	}
	
	PORTR.OUTCLR = 0x02;	// Turns the debug led on.  The led is active low
	PORTC_OUT = 0x03;		// turn on External LEDs on PC0, and LED on PC1
 
    while(1) {				// main Loop
		// TODO Code that runs forever in the robot
	}
}*/
