/* Openlights Strand Controller (12009) Firmware
 * http://openlights.org
 * 
 * Copyright (c) 2012 Jonathan Evans <jon@craftyjon.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <asf.h>
#include <board.h>
#include <conf_board.h>

#include "pins.h"
#include "globals.h"


// Setup I/O pins.
void init_pins(void)
{
	//ioport_enable_pin(SW0);
	//ioport_enable_pin(SW1);
	//ioport_enable_pin(SW2);
	//ioport_enable_pin(SW3);
	//ioport_enable_pin(SW4);
	//ioport_enable_pin(SW5);
		
	//ioport_set_pin_dir(SW0, IOPORT_DIR_INPUT);
	//ioport_set_pin_dir(SW1, IOPORT_DIR_INPUT);
	//ioport_set_pin_dir(SW2, IOPORT_DIR_INPUT);
	//ioport_set_pin_dir(SW3, IOPORT_DIR_INPUT);
	//ioport_set_pin_dir(SW4, IOPORT_DIR_INPUT);
	//ioport_set_pin_dir(SW5, IOPORT_DIR_INPUT);

	//ioport_set_pin_mode(SW0, IOPORT_MODE_PULLUP);
	//ioport_set_pin_mode(SW1, IOPORT_MODE_PULLUP);
	//ioport_set_pin_mode(SW2, IOPORT_MODE_PULLUP);
	//ioport_set_pin_mode(SW3, IOPORT_MODE_PULLUP);
	//ioport_set_pin_mode(SW4, IOPORT_MODE_PULLUP);
	//ioport_set_pin_mode(SW5, IOPORT_MODE_PULLUP);
	PORTB.DIRCLR =	0xFF;			//Set port B as inputs
	PORTB.DIR    =	0x00;			//Set port B as inputs
	PORTB.PIN0CTRL = PORT_OPC_PULLUP_gc;			//set pin 0 as a pull-up
	PORTB.PIN1CTRL = PORT_OPC_PULLUP_gc;			//set pin 1 as a pull-up
	PORTB.PIN2CTRL = PORT_OPC_PULLUP_gc;			//set pin 2 as a pull-up

	PORTA.DIRCLR =	0xFF ;			//Set port A
	PORTA.DIR =	0x00 ;			//Set port A 7,6,5,4 and 3 as inputs
	PORTA.PIN3CTRL = PORT_OPC_PULLUP_gc;			//set pin 3 as a pull-up
	PORTA.PIN4CTRL = PORT_OPC_PULLUP_gc;			//set pin 4 as a pull-up
	PORTA.PIN5CTRL = PORT_OPC_PULLUP_gc;			//set pin 5 as a pull-up
	PORTA.PIN6CTRL = PORT_OPC_PULLUP_gc;			//set pin 6 as a pull-up
	PORTA.PIN7CTRL = PORT_OPC_PULLUP_gc;			//set pin 7 as a pull-up

	ioport_enable_pin(RS485_REn);
	ioport_enable_pin(RS485_DE);
	
	ioport_set_pin_dir(RS485_REn, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(RS485_DE, IOPORT_DIR_OUTPUT);
	
	ioport_set_pin_low(RS485_REn);	// Active low enable receive
	ioport_set_pin_low(RS485_DE);
	
	//PORTA.DIRSET = (1<<6) | (1<<7);

	PORTC.DIRSET = PIN3_bm;
	PORTC.DIRCLR = PIN2_bm;

	// SPI pins for strand data output
	// PC5 = strand data
	// PC7 = strand data 2
	// PC0 LED0
	// PC1 LED1
	//PORTC.DIRSET =	0x03;			
	//External LEDs on PC0 & PC1	
	PORTC.DIRSET = (1<<0) |(1<<1) |(1<<5) | (1<<7);
	PORTC.PIN5CTRL = PORT_OPC_PULLUP_gc;
	PORTC.PIN7CTRL = PORT_OPC_PULLUP_gc;
}


// Setup global variables needed before communications starts
void init_globals(void)
{
	num_leds = 104;
	num_pixels = num_leds;
	num_bytes = 3*num_pixels;

	// Init board address
	g_address = 0x00;
	g_usbConnected = 0;
	
	if (!ioport_get_pin_level(MST) ) { //set this as transmitter/master
		g_usbConnected = 1;
		// Enable RS485 transmitter if USB is connected.
		// For now, this requires a power-cycle to clear.
		ioport_set_pin_high(RS485_DE); //Driver enable (on)
		ioport_set_pin_high(RS485_REn);	// NOT receiver enable (off)
		
	} else {
		ioport_set_pin_low(RS485_DE); //Driver enable (off)
		ioport_set_pin_low(RS485_REn); //NOT receiver enable (on)
		//read address from DIP
		if (!ioport_get_pin_level(SW0)) {
			g_address |= (1<<0);
		}
		
		if (!ioport_get_pin_level(SW1)) {
			g_address |= (1<<1);
		}
		
		if (!ioport_get_pin_level(SW2)) {
			g_address |= (1<<2);
		}
		
		if (!ioport_get_pin_level(SW3)) {
			g_address |= (1<<3);
		}
		
		//if (!ioport_get_pin_level(SW4)) {
			//g_address |= (1<<4);
		//}
		//
		//if (!ioport_get_pin_level(SW5)) {
			//g_address |= (1<<5);
		//}
	}
	// Init byte-oriented communications (via USB or RS485)
	g_cmdState = STATE_IDLE;
	g_usbDataLength = 0;
	g_usbDataCount = 0;
	g_rs485data = 0;
	g_rs485rdy = 0;
	g_rs232data = 0;
	g_rs232rdy = 0;

	endTime = 0L; // Latch timing reference
	// Init strand data buffers
	memset(&pixels, 0, sizeof(pixels));
	memset(&palette, 0, sizeof(palette));
	dirty = 0;
}


// RS485 is used for board-to-board communications
void init_rs485(void)
{
	struct usart_rs232_options usart_params;

	usart_params.baudrate = RS485_BIT_RATE;
	usart_params.charlength = USART_CHSIZE_8BIT_gc;
	usart_params.paritytype = USART_PMODE_DISABLED_gc;
	usart_params.stopbits = false;

	usart_init_rs232(&USARTC0, &usart_params);
	usart_set_rx_interrupt_level(&USARTC0, USART_INT_LVL_MED);
	usart_rx_enable(&USARTC0);
}
void init_rs232(void)
{
	struct usart_rs232_options usart_params;

	usart_params.baudrate = RS232_BIT_RATE;
	usart_params.charlength = USART_CHSIZE_8BIT_gc;
	usart_params.paritytype = USART_PMODE_DISABLED_gc;
	usart_params.stopbits = false;

	usart_init_rs232(&USARTE0, &usart_params);
	usart_set_rx_interrupt_level(&USARTE0, USART_INT_LVL_MED);
	usart_rx_enable(&USARTE0);
}

// SPI is used for strand data output
//void init_strand(void)
//{
	//struct spi_device dummy_device = {
		//.id = IOPORT_CREATE_PIN(PORTA, 1)
	//};
//
	//spi_master_init(&SPIC);
	//spi_master_setup_device(&SPIC, &dummy_device, SPI_MODE_0, STRAND_BIT_RATE, 0);
	//spi_enable(&SPIC);
//}
//
// Set 16MHZ clock
// Taken from : http://www.ikalogic.com/tips-tricks-atxmega-micro-controller/
void init_clock(void)
{
	//enable RC32M
	
	OSC.CTRL |= OSC_RC32MEN_bm ;
	//OSC.CTRL |= (1<<1); 
	//wait for 32M clock to stabilise
    while((OSC.STATUS & OSC_RC32MRDY_bm)==0){} // wait until stable
    //while((OSC.STATUS & (1<<1)) != (1<<1)) {;}

	//Prescaller
	
	CCP = CCP_IOREG_gc;				// protected write follows
	//CCP = 0xD8; //Allow Protected IO changing
	CLK.PSCTRL = CLK_PSADIV_2_gc;
	CLK.PSCTRL = 1; //set prescaller to 2 (/2)
	//CLK.PSCTRL = 0; //set prescaller to 1 (no division)
	
	DFLLRC32M.CTRL = DFLL_ENABLE_bm ;
	//DFLLRC32M_CTRL = 1; //activate auto calibration
	
	//PLL setup
	
	//OSC.PLLCTRL = 0b10000101; //use RC32M as input and multiple by 5
	//OSC.PLLCTRL = 0b00001000; //use 2M as input and multiple by 8 (net result, 16MHz)
	
	//OSC.CTRL |= (1<<4); //enable PLL
	//OSC.CTRL |= (0<<4); //disable PLL
	//while((OSC.STATUS & (1<<4)) != (1<<4)) {;}
	for(uint8_t i = 255; i!=0; i--){;}
	//
	//Set Clock
	
	
	CCP = CCP_IOREG_gc;				// protected write follows
	//CCP = 0xD8; //Allow Protected IO changing
	//CLK.CTRL = 0b00000100; //Select the PLL as clock
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc;	// The System clock is now the 32Mhz internal RC
	//CLK.CTRL = 0b00000001; //Select the RC32M as clock

}
// USB CDC class (i.e. virtual serial port) used for Controller
//void init_usb(void)
//{
	//udc_start();
	//udc_attach();
//}