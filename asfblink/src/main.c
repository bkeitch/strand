/* Openlights Strand Controller (12009) Firmware
 * http://openlights.org
 * 
 * Copyright (c) 2012 Jonathan Evans <jon@craftyjon.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <string.h>
#include <asf.h>
#include <util/delay.h>

#define GLOBALS_ALLOC
#include "globals.h"
#undef GLOBALS_ALLOC

#include "pins.h"
//#include "usb.h"
#include "commands.h"
#include "init.h"

#include "rainbow.h"
#include "ws2811.h"

/*
// High-frequency timer
static void strand_output_isr(void)
{
	if (dirty) {
		dirty = 0;
		
		PORTC.OUTSET = PIN1_bm;  // DATA
		show();
		//spi_write_packet(&SPIC, pixels, (3 * num_leds));
		PORTC.OUTCLR = PIN1_bm;  // DATA
	}
}

// Low-frequency timer
static void tick_isr(void)
{
	ticks++;
	
	// Refresh the strand periodically even if no new data
	if (g_cmdState == STATE_IDLE) {
		PORTC.OUTTGL = PIN1_bm;	// ACT
		PORTC.OUTCLR = PIN0_bm;	// DATA
		dirty = 1;
	}
	//usart_putchar(&USARTE0, '!');
}
*/

// RS485 receive ISR
ISR(USARTC0_RXC_vect)
{
	g_rs485data = USARTC0.DATA;
	g_rs485rdy = 1;
}
ISR(USARTE0_RXC_vect)
{
	g_rs232data = USARTE0.DATA;
	g_rs232rdy = 1;
}



int main(void) {

	// ASF board initialization
	sysclk_init();
	//Uncomment following to get stable 32MHZ clock (remember to set F_CLK in DEFINES)
	//init_clock();
	
	pmic_init();
	irq_initialize_vectors();
	cpu_irq_enable();
	// Openlights board initialization
	init_pins();
	init_globals();
	//init_strand(); //inits spi! Don't enable
	init_rs485();
	init_rs232(); //receiving data from RS232
	//init_usb();
	
	//This sets the two LEDs on the board off and on twice. Nice to know we are alive,
	//and gives me time to reset my scope probe :)
	
	blink_board_led();
	
	//Here for debugging, check clock is stable 32MHz
	//clock_test();
	
	
	// Setup timer interrupt for strand output
	//tc_enable(&TCC0);
	//tc_set_overflow_interrupt_callback(&TCC0, strand_output_isr);
	//tc_set_wgm(&TCC0, TC_WG_NORMAL);
	//tc_write_period(&TCC0, 2083);	// about 60 fps
	//tc_write_clock_source(&TCC0, TC_CLKSEL_DIV64_gc);	// 125 khz
	//tc_set_overflow_interrupt_level(&TCC0, TC_INT_LVL_HI);
	
	
	// Low-frequency timer
	//tc_enable(&TCC1);
	//tc_set_overflow_interrupt_callback(&TCC1, tick_isr);
	//tc_set_wgm(&TCC1, TC_WG_NORMAL);
	//tc_write_period(&TCC1, 0xFFFF);
	//tc_write_clock_source(&TCC1, TC_CLKSEL_DIV256_gc);
	//tc_set_overflow_interrupt_level(&TCC1, TC_INT_LVL_LO);
/*
	uint8_t command_count = 0;
	g_usbDataBuffer[0] = USB_START_BYTE;
	g_usbDataBuffer[1] = 0x00;
	g_usbDataBuffer[2] = SET_ALL_RGB;
	g_usbDataBuffer[3] = 128;
	g_usbDataBuffer[4] = 64;
	g_usbDataBuffer[5] = 0;
	uint8_t command_array[7]; // = {0x01, 0x02,0x04,0x08,0x10, 0x20, 0x40, 0x80};
	memcpy(command_array, g_usbDataBuffer, 6);
	command_array[6] = usb_check();
*/
	// run the "Identify" command at startup
	g_usbCommand = IDENTIFY;
	process_command();
	show();
	
	while (1) {
/************************************************************************/
/* 
* In descending order (RJ45 connectors top-most) pins are:

PB2 SW0 (PORTB, 2)
PB1 SW1 (PORTB, 1)
PB0 SW2 (PORTB, 0)
PA7 SW3 (PORTA, 7)
PA6 SW4 (PORTA, 6)
PA5 SW5 (PORTA, 5)
PA4 SW6 (PORTA, 4)
PA3 MST (PORTA, 3)


*/                                                                     
/************************************************************************/	
		//if ((~PORTB_IN & (1<<2)) != 0 ) {
			//PORTC.OUTSET = PIN1_bm | PIN0_bm;  // ACT & DATA
			//
		//}
		// all off via direct buffer
		//if ((~PORTB_IN & (1<<1)) != 0 ) {
			//PORTC.OUTSET = PIN1_bm;  // DATA
			//
			//for (uint16_t i = 0; i < num_leds; i++) {
				//pixels[i] = 0;
			//}
			//dirty = 1;
		//}
		//wipe red, green, blue
		//if ((~PORTB_IN & (1<<0)) != 0 ) {
			//blink_board_led();
			//colorWipe(Color(255, 0, 0), 50); // Red
			//colorWipe(Color(0, 255, 0), 50); // Green
			//colorWipe(Color(0, 0, 255), 50); // Blue
			//
		//}
		//all turquoise
		//if ((~PORTA_IN & (1<<7)) != 0 ) {
			//PORTC.OUTSET = PIN0_bm;  // ACT
			//for (uint16_t i = 0; i < num_leds; i++) {
				//setPixelColorI(i, 0, 32, 64);
			//}
			//dirty = 1;
		//}
		//water wave effect
		if ((~PORTA_IN & (1<<6)) != 0 ) {
			blink_board_led(); 
			for(uint8_t i = 0; i < 4; i++) {
				waterWave(120);
			}
		}
		//rainbow
		if ((~PORTA_IN & (1<<5)) != 0 ) {
			PORTC.OUTSET = PIN1_bm | PIN0_bm;  // ACT & DATA
			rainbow(20);
		}
		//rainbow cycle
		if ((~PORTA_IN & (1<<4)) != 0  ) {
			PORTC.OUTSET = PIN0_bm;  // ACT
			rainbowCycle(20);
		}
		if ((~PORTA_IN & (1<<4)) != 0 && g_usbConnected == 1 ) {
			PORTC.OUTSET = PIN0_bm;  // ACT ON
			usart_putchar(&USARTC0, TRIGGER);
			PORTC.OUTCLR = PIN0_bm;  // ACT OFF
		}
	
	
		//if (!ioport_get_pin_level(SW5)) {	// Test mode
		// else
		//if (udi_cdc_is_rx_ready()) { //wait for USB signal
		
		//PIN A3 set
		if (g_rs232rdy && g_usbConnected == 1) {
			PORTC.OUTSET = PIN0_bm;  // ACT ON
			g_rs232rdy = 0;
			//uint8_t usbbyte = command_array[command_count++];
			//if(command_count > 7) command_count = 0;
			usart_putchar(&USARTC0, g_rs232data);
			//usart_putchar(&USARTC0, usbbyte);
			//usart_putchar(&USARTE0, g_rs232data);		
			
			PORTC.OUTCLR = PIN0_bm;  // ACT OFF
		}
		
		if (g_rs485rdy && g_usbConnected != 1) {
			PORTC.OUTSET = PIN0_bm;  // ACT ON
			g_rs485rdy = 0;	
			//g_usbCommand = g_rs485data;
			//process_command();
		
			
			process_usb(g_rs485data);
			if(dirty) {
				show();
				dirty = 0;
			}
			
			PORTC.OUTCLR = PIN0_bm;  // ACT OFF
		}
		
	}
	
	return(0);


}
