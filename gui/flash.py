import serial
from random import random
from time import sleep
ser  = serial.Serial('/dev/ttyUSB0')
ser.baudrate = 115200
ser.write ('\x7a\0\x11\0\0\0')
def check_sum(c_array):
  calc = 0
  if (len(c_array) <= 1):
    return (c_array[0])
  for value in c_array:
    calc = calc ^ value
  return (calc)
palette = [[ 192, 128, 0 ], [ 255, 128, 0 ], [ 255, 64, 0 ], [ 192, 32, 0 ]]
while True:
  for colour in palette : 
    ser.write ('\x7a\0\x13\x03\0' + chr(colour[0]) + chr(colour[1]) + chr(colour[2]) + chr(check_sum(colour)))
    sleep(random()/5)

