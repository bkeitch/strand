import serial
NUM_LEDS = 52
START = 0x7A
SET_MAP = 0x09
SET_ARRAY = 0x10
BLACKOUT = 0x11
SET_ALL = 0x12
SET_ALL_RGB = 0x13
IDENTIFY = 0x14
RAINBOW = 0x17
WHEEL = 0x18
WATERWAVE = 0x19
COLOURWIPE = 0x20
BLINK_BOARD = 0x21
TRIGGER = 0xAA

#ser = serial.Serial('/dev/ttyUSB0') # Linux
ser = serial.Serial('COM10') #windows
ser.baudrate = 115200
	
	
# Calculates check sum for transmission. Checked at other end.
def check_sum(c_array):
  calc = 0
  if (len(c_array) <= 1):
    return (c_array[0])
  for value in c_array:
    calc = calc ^ value
  return (calc)

# Writes an array to the LED strip, either as given by the address, or to all the strips (address 0)
def setArray(colour_array, address = 0):
  ser.write(b'' + chr(START) + chr(address) + chr(SET_ARRAY) + chr(len(colour_array)) + '\x00') 
  for colour in colour_array :
    ser.write(b''+ chr(colour))
  chksum = check_sum(colour_array)
  ser.write(b''+chr(chksum))
  return;
# Function to look at output before sending to lights
def dumpArray(colour_array) :
  data_str = ''.join(str(e) for e in pixel_array)
  with open("leds.dat", "a") as f:
    f.write(data_str+'\n')

#Turns all LEDs off
def off() :
  ser.write(b'' + chr(START) + '\0' + chr(BLACKOUT) + '\0\0\0') 
#All strands will show binary address
def identify() :
  ser.write(b'' + chr(START) + '\0' + chr(IDENTIFY) + '\0\0\0') 
#Rainbow effect
def rainbow(delay) :
  ser.write(b'' + chr(START) + '\0' + chr(RAINBOW) + '\x01\0' + chr(delay) + chr(delay)) 
#Waterwave effect
def waterwave(delay) :
  ser.write(b'' + chr(START) + '\0' + chr(WATERWAVE) + '\x01\0' + chr(delay) + chr(delay)) 
#Wipe colour across all pixels
def colourwipe(colour, delay) :
  ser.write(b'' + chr(START) + '\0' + chr(COLOURWIPE) + '\x03\0'  + chr(colour[0]) + chr(colour[1])+ chr(colour[2])+ chr(check_sum(colour))) 
