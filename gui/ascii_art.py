from time import sleep
from strand import *

# Writes an array to the LED strip, either as given by the address, or to all the strips
def setArrayP(image_line, palette, address = 0):
  pixel_array = [0] * 150
  i = 0
  for pixel in image_line :
    rgb = palette[pixel]
    pixel_array[i] = rgb[2]
    pixel_array[i+1] = rgb[0]
    pixel_array[i+2] = rgb[1]
    i+=3
  setArray(pixel_array, address)
  
blue_palette = {' ' : [0,0,0], '#' : [0,0,0xFF]}
red_palette = {' ' : [0,0,0], '#': [0xFF,0,0]}
green_palette = {' ' : [0,0,0], '#' : [0,0xFF,0]}
palettes = [blue_palette, red_palette, green_palette]

# Loads ASCII art, e.g. as created by banner program. This can be inverted for R-> L scroll
# Also banner program does not append spaces to end of lines, so length can be made uniform by 
# setting uniform_length parameter
def loadImage(filename, invert = True, uniform_length = 0):
  image = []
  with open(filename, "r") as f:
    lines = f.read().splitlines()
  for line in lines :
    if uniform_length != 0 and len(line) != uniform_length:
      line += (' ' * (uniform_length - len(line))) 
    L = list (line)
    if invert:
      image.append (L[::-1])
    else:
      image.append (L)
  return(image)
def showImage(image, palette) : 
    line_number = 7
    for line in image : 
      setArrayP(line, palette, line_number)  
      line_number -= 1
 

# Scroll an image R->L. Note that image is mirrored in both X and Y ( row count reversed )
  
def scroll(image, palette = blue_palette) :
  for i in range ((len(image[0])-1),-1, -1) :
    line_number = 7
    for line in image : 
      setArrayP(line[i:(i+NUM_LEDS)], palette, line_number)  
      line_number -= 1
  sleep(0.05)

