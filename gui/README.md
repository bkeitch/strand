ascii_art.py : Python file to load and prepare ASCII art or text for scrolling etc.

convert.sh : script to output ASCII text or art to a Python array 
	
flash.py : flashes all pixels red, orange and yellow like fire
	
lightening_bolt.py : flashes some red pixels
	
oxburn.py : Scrolls oxburn in different colours. The old way (data in file)
	
pacman.py : Easy to guess
	
sophia.py : For Sophie
	
strand.py : main file. Use "import strand" to get the library functions (sending an array to the LEDs). Note that there is a "dumpArray()" function to see data.
	
