from time import sleep
from strand import *
  
# Example: flash Red bolts!
# Set the matching LED at position i=j to red. Blank all others.
colour_array = [0] * 150
for k in range (0,5):
  for i in range (0,50):
    for j in range (0,50):
      if (i == j):
        colour_array[j*3] = 0xFF # red at position
        colour_array[j*3+1] = 0xFF # red at position
        colour_array[j*3+2] = 0xFF # red at position
      else :
        colour_array[j*3] = 0x00 # blank all other positions
        colour_array[j*3+1] = 0x00  # no green
        colour_array[j*3+2] = 0x00  # no blue
    setArray(colour_array)  
    sleep(0.5)
ser.close()
