#include <stdio.h>
#include <math.h>
#include <stdint.h>
#define max fmax
uint32_t Color(uint8_t r, uint8_t g, uint8_t b) {
	return ((uint32_t)b << 16) | ((uint32_t)r << 8) | g;
}
uint8_t x,y;
void delay_ms(int i) {}
void set_colour(uint32_t j) {

// (uint8_t)(j >> 8), (uint8_t)(j) , (uint8_t)(j >> 16)
	printf("<rect  x='%u' y='%u' width='10' height='10' style='fill:rgb(%u,%u,%u);' />\n",x,y,(uint8_t)(j >> 16),(uint8_t)(j >> 8), (uint8_t)(j) );
	x+=10;
	if(x>100) {
		x=0;
		y+=10;
	}
}
void colour_fade (uint32_t start, uint32_t end, uint8_t wait) {
	uint32_t j;
	float incr;
	if(start< end) {
		incr = (end - start)/64.0;
		for(j=start; j<end; j+=max((int)incr,0)) { // 5 cycles of all colors on wheel

			set_colour(j);
			
			delay_ms(wait);

		}
	} else {
		incr = (start - end)/64.0;
		for(j=start; j>end; j-=max((int)incr,0)) { // 5 cycles of all colors on wheel

			set_colour(j);
			
			delay_ms(wait);

		}
	}
	
}
int main() {
	x=0;
	y=0;
	printf("<html>\
<body>\
<h1>My first SVG</h1><svg width='300' height='300'>\n");
	// G B R
	colour_fade(Color(128,0,255), Color(64,0,192), 100);
	colour_fade(Color(64,0,192), Color(0,64,128), 100);
	colour_fade(Color(0,64,128), Color(64,0,192), 100);
	colour_fade(Color(64,0,192), Color(128,0,255), 100);
	printf("</svg>\
	</body>\
</html> \n");
	//colour_fade(Color(128,0,64),Color(0,0,0), 25);
	//colour_fade(Color(0,0,0), Color(128,0,64), 25);
	return(0);
}
