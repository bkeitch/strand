from ascii_art import *
img1 = loadImage("ross", True, 40)
img2 = loadImage("and", True, 40)
img3 = loadImage("ben", True, 40)
blue_palette = {' ' : [0,0,0], '#' : [0,0,0xFF]}
red_palette = {' ' : [0,0,0], '#': [0xFF,0,0]}
green_palette = {' ' : [0,0,0], '#' : [0,0xFF,0]}
scroll(img1, blue_palette)
scroll(img2, red_palette)
scroll(img3, green_palette)
