#!/bin/bash
# Uses banner to convert # into a colour. This is reversed and
# then the line order must be flipped for scrolling from 0th end
echo > data.py
for i in 1 2 3 4 5 6 7 
do
  sed -n "$i"p raw| perl -ne '$a = reverse; print $a;' |\
  sed -e 's/#/"b",/g; s/\s/"k",/g; s/^/data\['"$i"'\] = \[/; s/,$/]/' >> data.py
done
#Old way
#banner word | perl -ne '$a = reverse; print $a;' |\
 sed -e 's/#/255,0,64,/g; s/\s/0,0,0,/g; s/^/data_1 = [/; s/,$/]/' >> data.py
 