PLIBRARY
========

This is the Processing Library for the strand project. It creates a library for the Processing Language that can be called like:

    import strand.*;

    PStrand st; // hardware object that represents connected LEDs
    st.setAll(colour_array);

where `colour_array` is an array of ints each representing a packed array of RGB bytes.

To build the library install a JDK and run the make file. This will generate a zip file to be copied into the Processing/libraries folder
