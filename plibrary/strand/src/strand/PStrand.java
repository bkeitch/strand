
  /**
 * PStrand
 * This class deals with interfacing between the user code and the LED strands
 * the communication is done over a serial port (initalised elsewhere)
 * if a null serial port is given, the code will not do anything
 * 
 * The main purpose is to convert an array of RGB values into data
 * that is sent to the LED Strands, one at a time. It is also an interface to
 * in-built effects on the Strand board
 */
 
package strand;
import processing.core.*;
import processing.serial.*;

public class PStrand  {
  static byte START = 0x7A;
  static byte SET_MAP = 0x09;
  static byte SET_ARRAY = 0x10;
  static byte BLACKOUT = 0x11;
  static byte SET_ALL = 0x12;
  static byte SET_ALL_RGB = 0x13;
  static byte IDENTIFY = 0x14;
  static byte RAINBOW = 0x17;
  static byte WHEEL = 0x18;
  static byte WATERWAVE = 0x19;
  static byte COLOURWIPE = 0x20;
  static byte BLINK_BOARD = 0x21;
  static byte TRIGGER = (byte) 0xAA; //cast because byte in Java is signed. 
  private Serial ser;
  PApplet parent;
  //int[] ledPixels = new int[LEDS*STRANDS];
  private int delay = 100;


  public PStrand(PApplet parent) {
    this.parent = parent;
    parent.registerMethod("dispose", this);

  }
  /*
   * method to set serial port which must be set up in the setup function in the main Window
   */
  public void setSerial (Serial s) {
    
    this.ser = s;
  }
  /**
   * set a delay value for the built-in effects. This is only because 
   * the thread method in Processing can not take a parameter, so we have to 
   * use a setter instead
   */
  public void setDelay(int delay) {
	  this.delay = delay;
  }
  /** Copies colour values into object but does not write them to the 
   * lights
   * For future purposes
   */
  public void setPixels(int[] colours) {
//   arrayCopy(colours,ledPixels);
  }
  /** Calculates check sum for transmission. Checked at other end.
   * Uses a simple XOR function across all bytes
   */
  private byte checkSum(byte[] c_array) {
    byte calc = 0;
    if (c_array.length <= 1) {
      return (c_array[0]);
    }
    for (int i = 0; i < c_array.length; i++) {
      calc ^= c_array[i];
    }
    return (calc);
  }
  /** convenience method to setArray for strand 0 (is this used?)  
  */
  public void setArray(int[] colour_array) {
    this.setArray(colour_array, 0);
  }
  /** Set all pixels in an array of strands 
  */
  public void setAll(int[][] colour_array) {
    for(int j = 0 ; j < colour_array.length; j++) {
      setArray(colour_array[j] , colour_array.length - j);
    }
  }
  /** Writes an array of Pixels (RGB int or Pixel color) values to the Strand, either as given by the address of the strands, 
  * or to all the strips (address 0)
  */
  public void setArray(int[] colour_array, int strand) {
    byte[] colours = new byte[3*colour_array.length];
    int j = 0;
    for (int i = colour_array.length - 1 ; i >=0 ; i--) {
      colours[j++] = (byte) Colour.red(colour_array[i]);
      colours[j++] = (byte) Colour.green(colour_array[i]);
      colours[j++] = (byte) Colour.blue(colour_array[i]);
    }
    this.setArray(colours, (byte) strand);
  }
  /** Writes an array of RGB bytes (unpacked colour values) to the LED strip, either as given by the address, 
  * or to all the strips (address 0)
  */
  public void setArray(byte[] colour_array, byte address) {
    byte[] header = {START, address, SET_ARRAY, (byte) colour_array.length, 0};
    write(header);
    write(colour_array);
    byte chksum = checkSum(colour_array);
    write(chksum);
  }
  /** Function to look at output (as packed RGB bytes) before sending to lights
   * useful for debugging
   */
  public String dumpArray(byte[] colour_array) {
    String result = new String();
    for (int i = 0; i < colour_array.length; i++) {
      result += (String.format("%02X ", colour_array[i]) + ":");
      if(((i+1)%3)==0) parent.print("  ");
    }
    return result;
  }
  /** Function to look at output (as ints) before sending to lights
  */
  public String dumpArray(int[] colour_array) {
    byte[] colours = new byte[3*colour_array.length];
    int j = 0;
    for (int i = colour_array.length - 1 ; i >=0 ; i--) {
      colours[j++] = (byte) Colour.red(colour_array[i]);
      colours[j++] = (byte) Colour.green(colour_array[i]);
      colours[j++] = (byte) Colour.blue(colour_array[i]);
    }
    return this.dumpArray(colours);
  }
  /** Turns all LEDs off using hardware function
  */
  public void off() {
    byte[] data = {START, 0, BLACKOUT, 0, 0, 0};
    write(data);
  }
  /** All strands will show their individual binary address. Very useful in debugging installations.
  */
  public void identify() {
    byte[] data = {START, 0, IDENTIFY, 0, 0, 0};
    write(data);
  }
  /** Rainbow effect (is built-in to firmware)
  */
  public void rainbow() {
    byte[] data = {START, 0, RAINBOW, 1, 0, (byte) delay, (byte) delay};
    write(data);
  }
  /**Waterwave effect (is built-in to firmware)
  */
  public void waterwave() {
    byte[] data = {START, 0, WATERWAVE, 1, 0, (byte) delay, (byte) delay};
    write(data);
  }
  /**Wipe colour across all pixels (is built-in to firmware)
  */
  public void colourwipe (int c) {
    byte[] colours = {(byte) Colour.red(c), (byte) Colour.green(c), (byte) Colour.blue(c)};
    byte[] data = {START, 0, COLOURWIPE, 3, 0, (byte) Colour.red(c), (byte) Colour.green(c), (byte) Colour.blue(c), checkSum(colours)};
    write(data);
  
  }
  /** write data array to the serial port
  */
  private void write(byte [] data) {
    if (ser != null) {
      ser.write(data);
    } else {
      //window.display(data);
    }
  }
  /** write individual byte to serial port
  */
  private void write(byte data) {
    if (ser != null) {
      ser.write(data);
    } else {
      //window.display(data);
    }
  }
   /** Anything in here will be called automatically when 
    * the parent sketch shuts down. For instance, this might
    * shut down a thread used by this library.
	* Currently does nothing
	*/
  public void dispose() {
  }


  class Colour {
	  private int colour;
	  Colour (int colour) {
		  this.colour = colour;
	  }
	  /** returns red part of RGB packed int
	  */
	  public static byte red(int colour) {
		return (byte) (colour & 0xFF);
	  }
	  /** returns green part of RGB packed int
	  */
	  public static byte green(int colour) {
	   return (byte) ((colour >> 16) & 0xFF);
	  }
	  /** returns blue part of RGB packed int
	  */
	  public static byte blue(int colour) {
		return (byte) ((colour >> 8) & 0xFF);
	  }
	  public String toString() { 
		return ("R:"+red(colour)+",G:"+green(colour)+",B:"+blue(colour));
	  }
  }
}