#Enter project directory 
cd strand/src/
export PPATH=~/winhome/Documents/processing-4.3
#Compile, using path to Processing libraries
javac -classpath $PPATH/core/library/core.jar:$PPATH/modes/java/libraries/serial/library/serial.jar:$PPATH/modes/java/libraries/serial/library/jssc.jar strand/PStrand.java

#Create JAR file being sure to get right relative paths
jar cfvm ../library/strand.jar manifest.txt strand/PStrand.class 
#Create ZIP file for Processing again, ensuring correct relative paths
cd ../..
zip strand.zip strand/library/export.txt strand/library/strand.jar strand/library.properties 
# Deploy into current user's Processing library folder
#cd ../../../Documents/Processing/libraries/
#unzip ../../../Git/strand/plibrary/strand.zip 
