Strand Lighting Controller
==========================

Based on openlights.org code.

This controls serial lighting strips that use NRZ format and not SPI. 

It uses boards that do not have in-build USB, and needs an external RS232 connection.

Effects are pre-programmed on the board for speed.


